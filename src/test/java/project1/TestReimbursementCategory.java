package project1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dev.harris.enums.ReimbursementCategory;

public class TestReimbursementCategory {

    @Test
    public void testGetCode() {
	assertEquals(ReimbursementCategory.BUSINESS.getCode(), 0);
	assertEquals(ReimbursementCategory.TRAVEL.getCode(), 1);
	assertEquals(ReimbursementCategory.MILEAGE.getCode(), 2);
	assertEquals(ReimbursementCategory.OTHER.getCode(), 3);
    }

    @Test
    public void testToString() {
	assertEquals(ReimbursementCategory.BUSINESS.toString(), "Business");
	assertEquals(ReimbursementCategory.TRAVEL.toString(), "Travel");
	assertEquals(ReimbursementCategory.MILEAGE.toString(), "Mileage");
	assertEquals(ReimbursementCategory.OTHER.toString(), "Other");
    }

    @Test(expected = NullPointerException.class)
    public void testToReimbursementCategoryNullArgument() {
	ReimbursementCategory.toReimbursementCategory(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToReimbursementCategoryIllegalArgument() {
	ReimbursementCategory.toReimbursementCategory("Tacos");
    }

    @Test
    public void testToReimbursementCategory() {
	assertEquals(ReimbursementCategory.toReimbursementCategory("Business"), ReimbursementCategory.BUSINESS);
	assertEquals(ReimbursementCategory.toReimbursementCategory("BUSINESS"), ReimbursementCategory.BUSINESS);
	assertEquals(ReimbursementCategory.toReimbursementCategory("business"), ReimbursementCategory.BUSINESS);

	assertEquals(ReimbursementCategory.toReimbursementCategory("Travel"), ReimbursementCategory.TRAVEL);
	assertEquals(ReimbursementCategory.toReimbursementCategory("TRAVEL"), ReimbursementCategory.TRAVEL);
	assertEquals(ReimbursementCategory.toReimbursementCategory("travel"), ReimbursementCategory.TRAVEL);

	assertEquals(ReimbursementCategory.toReimbursementCategory("Mileage"), ReimbursementCategory.MILEAGE);
	assertEquals(ReimbursementCategory.toReimbursementCategory("MILEAGE"), ReimbursementCategory.MILEAGE);
	assertEquals(ReimbursementCategory.toReimbursementCategory("mileage"), ReimbursementCategory.MILEAGE);

	assertEquals(ReimbursementCategory.toReimbursementCategory("Other"), ReimbursementCategory.OTHER);
	assertEquals(ReimbursementCategory.toReimbursementCategory("OTHER"), ReimbursementCategory.OTHER);
	assertEquals(ReimbursementCategory.toReimbursementCategory("other"), ReimbursementCategory.OTHER);
    }

}
