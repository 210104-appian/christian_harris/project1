package dev.harris.user;

public class Credentials {
    private final String username;
    private final String password;

    public Credentials() {
	this.username = null;
	this.password = null;
    }

    public Credentials(String username, String password) {
	this.username = username;
	this.password = password;
    }

    public String getUsername() {
	return this.username;
    }

    public String getPassword() {
	return this.password;
    }

    @Override
    public String toString() {
	return "Credentials [username=" + this.username + ", password=" + this.password + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = (prime * result) + ((this.password == null) ? 0 : this.password.hashCode());
	result = (prime * result) + ((this.username == null) ? 0 : this.username.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (this.getClass() != obj.getClass()) {
	    return false;
	}
	final Credentials other = (Credentials) obj;
	if (this.password == null) {
	    if (other.password != null) {
		return false;
	    }
	} else if (!this.password.equals(other.password)) {
	    return false;
	}
	if (this.username == null) {
	    if (other.username != null) {
		return false;
	    }
	} else if (!this.username.equals(other.username)) {
	    return false;
	}
	return true;
    }
}
