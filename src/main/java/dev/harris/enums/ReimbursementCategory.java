package dev.harris.enums;

import org.apache.log4j.Logger;

/**
 * Describes a named constant for tracking categories of reimbursment requests.
 *
 * @author Christian Harris
 *
 */
public enum ReimbursementCategory {
    BUSINESS(0, "Business"),

    TRAVEL(1, "Travel"),

    MILEAGE(2, "Mileage"),

    OTHER(3, "Other");

    private static Logger logger = Logger.getRootLogger();
    private final int code;
    private final String name;

    private ReimbursementCategory(int code, String name) {
	this.code = code;
	this.name = name;
    }

    /**
     * Gets the integer representation of this code.
     *
     * @return code - the integer representation of this code.
     */
    public int getCode() {
	// A log is made any time this method is called.
	logger.info("Getting a reimbursment request code.");
	return this.code;
    }

    /**
     * Gets a string representing the named context of this code.
     *
     * @return name - the named context of this code.
     */
    @Override
    public String toString() {
	// A log is made any time this method is called.
	logger.info("Getting a contextual name of a reimbursment request.");
	return this.name;
    }

    /**
     * Converts a String to a ReimbursmentCategory.
     *
     * @param string - the String to be converted.
     * @return - the ReimbursmentCategory constant associated with the String
     *         string.
     * @throws NullPointerException     - if string is null.
     * @throws IllegalArgumentException - if string does not represent a
     *                                  ReimbursmentCategory;
     */
    public static ReimbursementCategory toReimbursementCategory(String string)
	    throws NullPointerException, IllegalArgumentException {
	// A log is made any time this method is called.
	logger.info("Converting a string to a reimbursment request constant.");
	// If string is not null we attempt to convert. If no conversion is possible we
	// throw an illegal argument exception.
	if (string != null) {
	    if (string.equalsIgnoreCase("BUSINESS")) {
		return ReimbursementCategory.BUSINESS;
	    } else if (string.equalsIgnoreCase("TRAVEL")) {
		return ReimbursementCategory.TRAVEL;
	    } else if (string.equalsIgnoreCase("MILEAGE")) {
		return ReimbursementCategory.MILEAGE;
	    } else if (string.equalsIgnoreCase("OTHER")) {
		return ReimbursementCategory.OTHER;
	    } else {
		logger.info("Throwing an illegal argument exception.");
		throw new IllegalArgumentException(string + " is not a valid reimbursment category.");
	    }
	}
	// If string is null we throw a null pointer exception.
	else {
	    logger.info("Throwing a null pointer exception.");
	    throw new NullPointerException("string is null.");
	}
    }
}
