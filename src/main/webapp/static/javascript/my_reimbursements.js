const myReimbursementsUrl = baseUrl + "/myreimbursements?"


function renderReimbursements(filter){
    divContainer = document.getElementById("pending-reimbursements-div");

    while(divContainer.firstChild){
        divContainer.removeChild(divContainer.firstChild);
    }

    divContainer = document.getElementById("resolved-reimbursements-div");

    while(divContainer.firstChild){
        divContainer.removeChild(divContainer.firstChild);
    }

    let all = document.getElementById("status-selector-all");
    let pending = document.getElementById("status-selector-pending");
    let resolved = document.getElementById("status-selector-resolved");

    if(filter == "all"){
        all.className = "status-selector-active";
        pending.className = "status-selector";
        resolved.className = "status-selector";
    }
    else if(filter == "pending"){
        all.className = "status-selector";
        pending.className = "status-selector-active";
        resolved.className = "status-selector";
    }
    else if(filter == "resolved"){
        all.className = "status-selector";
        pending.className = "status-selector";
        resolved.className = "status-selector-active";
    }

    let selection = filter;
    let id = sessionStorage.getItem("token").split(":")[0];
    let url = myReimbursementsUrl + "selection=" + selection + "&id=" + id;
    performAjaxGetRequest(url, processReimbursementRequests);
}

function processReimbursementRequests(jsonReimbursementRequests){
    reimbursementRequests = JSON.parse(jsonReimbursementRequests);

    for(let reimbursementRequest of reimbursementRequests){
        let divContainer;
        if(reimbursementRequest.dateResolved == null){
            divContainer = document.getElementById("pending-reimbursements-div");

            let reimbursementDiv = document.createElement("div");
            reimbursementDiv.setAttribute("class", "reimbursement-div");

            let dateMadeDiv = document.createElement("div");
            dateMadeDiv.setAttribute("class", "reimbursement-item-div");
            let dateMadeLabel = document.createElement("p");
            dateMadeLabel.setAttribute("class", "reimbursement-item-label");
            dateMadeLabel.innerHTML = "Date Made";
            let dateMadeParagraph = document.createElement("p");
            dateMadeParagraph.setAttribute("class", "reimbursement-item-paragraph");
            dateMadeParagraph.innerHTML = reimbursementRequest.dateMade;
            dateMadeDiv.appendChild(dateMadeLabel);
            dateMadeDiv.appendChild(dateMadeParagraph);

            let descriptionDiv = document.createElement("div");
            descriptionDiv.setAttribute("class", "reimbursement-item-div");
            let descriptionLabel = document.createElement("p");
            descriptionLabel.setAttribute("class", "reimbursement-item-label");
            descriptionLabel.innerHTML = "Description";
            let descriptionParagraph = document.createElement("p");
            descriptionParagraph.setAttribute("class", "reimbursement-item-paragraph");
            descriptionParagraph.innerHTML = reimbursementRequest.description;
            descriptionDiv.appendChild(descriptionLabel);
            descriptionDiv.appendChild(descriptionParagraph);

            let categoryDiv = document.createElement("div");
            categoryDiv.setAttribute("class", "reimbursement-item-div");
            let categoryLabel = document.createElement("p");
            categoryLabel.setAttribute("class", "reimbursement-item-label");
            categoryLabel.innerHTML = "Category";
            let categoryParagraph = document.createElement("p");
            categoryParagraph.setAttribute("class", "reimbursement-item-paragraph");
            categoryParagraph.innerHTML = reimbursementRequest.category;
            categoryDiv.appendChild(categoryLabel);
            categoryDiv.appendChild(categoryParagraph);

            let costDiv = document.createElement("div");
            costDiv.setAttribute("class", "reimbursement-item-div");
            let costLabel = document.createElement("p");
            costLabel.setAttribute("class", "reimbursement-item-label");
            costLabel.innerHTML = "Cost";
            let costParagraph = document.createElement("p");
            costParagraph.setAttribute("class", "reimbursement-item-paragraph");
            costParagraph.innerHTML = "$" + reimbursementRequest.cost;
            costDiv.appendChild(costLabel);
            costDiv.appendChild(costParagraph);

            reimbursementDiv.appendChild(dateMadeDiv);
            reimbursementDiv.appendChild(document.createElement("div"));
            reimbursementDiv.appendChild(descriptionDiv);
            reimbursementDiv.appendChild(categoryDiv);
            reimbursementDiv.appendChild(costDiv);

            divContainer.appendChild(reimbursementDiv);
        }
        else{
            divContainer = document.getElementById("resolved-reimbursements-div");

            let reimbursementDiv = document.createElement("div");
            reimbursementDiv.setAttribute("class", "reimbursement-div");

            let managerDiv = document.createElement("div");
            managerDiv.setAttribute("class", "reimbursement-item-div");
            let managerLabel = document.createElement("p");
            managerLabel.setAttribute("class", "reimbursement-item-label");
            managerLabel.innerHTML = "Manager";
            let managerParagraph = document.createElement("p");
            managerParagraph.setAttribute("class", "reimbursement-item-paragraph");
            managerParagraph.innerHTML = reimbursementRequest.managerId;
            managerDiv.appendChild(managerLabel);
            managerDiv.appendChild(managerParagraph);

            let dateMadeDiv = document.createElement("div");
            dateMadeDiv.setAttribute("class", "reimbursement-item-div");
            let dateMadeLabel = document.createElement("p");
            dateMadeLabel.setAttribute("class", "reimbursement-item-label");
            dateMadeLabel.innerHTML = "Date Made";
            let dateMadeParagraph = document.createElement("p");
            dateMadeParagraph.setAttribute("class", "reimbursement-item-paragraph");
            dateMadeParagraph.innerHTML = reimbursementRequest.dateMade;
            dateMadeDiv.appendChild(dateMadeLabel);
            dateMadeDiv.appendChild(dateMadeParagraph);

            let dateResolvedDiv = document.createElement("div");
            dateResolvedDiv.setAttribute("class", "reimbursement-item-div");
            let dateResolvedLabel = document.createElement("p");
            dateResolvedLabel.setAttribute("class", "reimbursement-item-label");
            dateResolvedLabel.innerHTML = "Date Resolved";
            let dateResolvedParagraph = document.createElement("p");
            dateResolvedParagraph.setAttribute("class", "reimbursement-item-paragraph");
            dateResolvedParagraph.innerHTML = reimbursementRequest.dateResolved;
            dateResolvedDiv.appendChild(dateResolvedLabel);
            dateResolvedDiv.appendChild(dateResolvedParagraph);

            let descriptionDiv = document.createElement("div");
            descriptionDiv.setAttribute("class", "reimbursement-item-div");
            let descriptionLabel = document.createElement("p");
            descriptionLabel.setAttribute("class", "reimbursement-item-label");
            descriptionLabel.innerHTML = "Description";
            let descriptionParagraph = document.createElement("p");
            descriptionParagraph.setAttribute("class", "reimbursement-item-paragraph");
            descriptionParagraph.innerHTML = reimbursementRequest.description;
            descriptionDiv.appendChild(descriptionLabel);
            descriptionDiv.appendChild(descriptionParagraph);

            let categoryDiv = document.createElement("div");
            categoryDiv.setAttribute("class", "reimbursement-item-div");
            let categoryLabel = document.createElement("p");
            categoryLabel.setAttribute("class", "reimbursement-item-label");
            categoryLabel.innerHTML = "Category";
            let categoryParagraph = document.createElement("p");
            categoryParagraph.setAttribute("class", "reimbursement-item-paragraph");
            categoryParagraph.innerHTML = reimbursementRequest.category;
            categoryDiv.appendChild(categoryLabel);
            categoryDiv.appendChild(categoryParagraph);

            let costDiv = document.createElement("div");
            costDiv.setAttribute("class", "reimbursement-item-div");
            let costLabel = document.createElement("p");
            costLabel.setAttribute("class", "reimbursement-item-label");
            costLabel.innerHTML = "Cost";
            let costParagraph = document.createElement("p");
            costParagraph.setAttribute("class", "reimbursement-item-paragraph");
            costParagraph.innerHTML = "$" + reimbursementRequest.cost;
            costDiv.appendChild(costLabel);
            costDiv.appendChild(costParagraph);

            let approvedDiv = document.createElement("div");
            approvedDiv.setAttribute("class", "reimbursement-item-div");
            let approvedLabel = document.createElement("p");
            approvedLabel.setAttribute("class", "reimbursement-item-label");
            approvedLabel.innerHTML = "Approved";
            let approvedParagraph = document.createElement("p");
            approvedParagraph.setAttribute("class", "reimbursement-item-paragraph");
            approvedParagraph.innerHTML = reimbursementRequest.approved;
            approvedDiv.appendChild(approvedLabel);
            approvedDiv.appendChild(approvedParagraph);

            reimbursementDiv.appendChild(managerDiv);
            reimbursementDiv.appendChild(dateMadeDiv);
            reimbursementDiv.appendChild(dateResolvedDiv);
            reimbursementDiv.appendChild(descriptionDiv);
            reimbursementDiv.appendChild(categoryDiv);
            reimbursementDiv.appendChild(costDiv);
            reimbursementDiv.appendChild(approvedDiv);

            divContainer.appendChild(reimbursementDiv);
        }

    }
}