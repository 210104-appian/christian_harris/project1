
function renderRequestsForApproval(){
    let id = sessionStorage.getItem("token").split(":")[0];
    let url = baseUrl + "/managereimbursements?id=" + id;
    performAjaxGetRequest(url, renderRequests);
}

function renderRequests(jsonReimbursementRequests){
    reimbursementRequests = JSON.parse(jsonReimbursementRequests);

    for(let reimbursementRequest of reimbursementRequests){
        let divContainer = document.getElementById("pending-reimbursements-div");

        let reimbursementDiv = document.createElement("div");
        reimbursementDiv.setAttribute("class", "reimbursement-div");

        let employeeDiv = document.createElement("div");
        employeeDiv.setAttribute("class", "reimbursement-item-div");
        let employeeLabel = document.createElement("p");
        employeeLabel.setAttribute("class", "reimbursement-item-label");
        employeeLabel.innerHTML = "Employee";
        let employeeParagraph = document.createElement("p");
        employeeParagraph.setAttribute("class", "reimbursement-item-paragraph");
        employeeParagraph.innerHTML = reimbursementRequest.employeeId;
        employeeDiv.appendChild(employeeLabel);
        employeeDiv.appendChild(employeeParagraph);

        let dateMadeDiv = document.createElement("div");
        dateMadeDiv.setAttribute("class", "reimbursement-item-div");
        let dateMadeLabel = document.createElement("p");
        dateMadeLabel.setAttribute("class", "reimbursement-item-label");
        dateMadeLabel.innerHTML = "Date Made";
        let dateMadeParagraph = document.createElement("p");
        dateMadeParagraph.setAttribute("class", "reimbursement-item-paragraph");
        dateMadeParagraph.innerHTML = reimbursementRequest.dateMade;
        dateMadeDiv.appendChild(dateMadeLabel);
        dateMadeDiv.appendChild(dateMadeParagraph);

        let descriptionDiv = document.createElement("div");
        descriptionDiv.setAttribute("class", "reimbursement-item-div");
        let descriptionLabel = document.createElement("p");
        descriptionLabel.setAttribute("class", "reimbursement-item-label");
        descriptionLabel.innerHTML = "Description";
        let descriptionParagraph = document.createElement("p");
        descriptionParagraph.setAttribute("class", "reimbursement-item-paragraph");
        descriptionParagraph.innerHTML = reimbursementRequest.description;
        descriptionDiv.appendChild(descriptionLabel);
        descriptionDiv.appendChild(descriptionParagraph);

        let categoryDiv = document.createElement("div");
        categoryDiv.setAttribute("class", "reimbursement-item-div");
        let categoryLabel = document.createElement("p");
        categoryLabel.setAttribute("class", "reimbursement-item-label");
        categoryLabel.innerHTML = "Category";
        let categoryParagraph = document.createElement("p");
        categoryParagraph.setAttribute("class", "reimbursement-item-paragraph");
        categoryParagraph.innerHTML = reimbursementRequest.category;
        categoryDiv.appendChild(categoryLabel);
        categoryDiv.appendChild(categoryParagraph);

        let costDiv = document.createElement("div");
        costDiv.setAttribute("class", "reimbursement-item-div");
        let costLabel = document.createElement("p");
        costLabel.setAttribute("class", "reimbursement-item-label");
        costLabel.innerHTML = "Cost";
        let costParagraph = document.createElement("p");
        costParagraph.setAttribute("class", "reimbursement-item-paragraph");
        costParagraph.innerHTML = "$" + reimbursementRequest.cost;
        costDiv.appendChild(costLabel);
        costDiv.appendChild(costParagraph);

        let buttonDiv = document.createElement("div");
        buttonDiv.setAttribute("id", "approve-deny-button-div");

        let approveButton = document.createElement("input")
        approveButton.setAttribute("type", "button");
        approveButton.setAttribute("value", "Approve");
        approveButton.setAttribute("id", "btn-approve:" + reimbursementRequest.id);
        approveButton.setAttribute("class", "approve-deny-buttons");
        approveButton.onclick = function approveRequest(){
            let url = baseUrl + "/managereimbursements?type=approve"
            performAjaxPutRequest(url, reimbursementRequest.id + ":" + sessionStorage.getItem("token").split(":")[0]);
        }

        let denyButton = document.createElement("input")
        denyButton.setAttribute("type", "button");
        denyButton.setAttribute("value", "Deny");
        denyButton.setAttribute("id", "btn-deny:" + reimbursementRequest.id);
        denyButton.setAttribute("class", "approve-deny-buttons");
        denyButton.onclick = function denyRequest(){
            let url = baseUrl + "/managereimbursements?type=deny"
            performAjaxPutRequest(url, reimbursementRequest.id + ":" + sessionStorage.getItem("token").split(":")[0]);
        }

        buttonDiv.appendChild(approveButton);
        buttonDiv.appendChild(denyButton);

        reimbursementDiv.appendChild(employeeDiv);
        reimbursementDiv.appendChild(dateMadeDiv);
        reimbursementDiv.appendChild(document.createElement("div"));
        reimbursementDiv.appendChild(descriptionDiv);
        reimbursementDiv.appendChild(categoryDiv);
        reimbursementDiv.appendChild(costDiv);
        reimbursementDiv.appendChild(buttonDiv);

        divContainer.appendChild(reimbursementDiv);
    }
}