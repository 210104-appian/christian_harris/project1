function submitNewReimbursement(){

    let descriptionValue = document.getElementById("description-text-area").value;
    let categoryValue = document.getElementById("category-select").value;
    let costValue = document.getElementById("cost-input").value;

    let descriptionValid = false;
    let categoryValid = false;
    let costValid = false;

    if(descriptionValue != ""){
        descriptionValid = true;
    }

    if(categoryValue == "BUSINESS" || categoryValue == "MILEAGE" || categoryValue == "TRAVEL" || categoryValue == "OTHER"){
        categoryValid = true;
    }

    if(!costValue.includes("e") && !costValue.includes("-") && costValue != null && costValue != ""){
        costValid = true;
    }

    if(descriptionValid && categoryValid && costValid){
        let tokenId = sessionStorage.getItem("token").split(":")[0];
        let newReimbursement = { employeeId: tokenId, description: descriptionValue, category: categoryValue, cost: costValue };
        let newReimbursementJson = JSON.stringify(newReimbursement);
        let newReimbursementUrl = baseUrl + "/newreimbursement";
        performAjaxPutRequest(newReimbursementUrl, newReimbursementJson)
    }
    else{
        if(!descriptionValid){
            document.getElementById("description-error-msg").hidden = false;
        }
        if(!categoryValid){
            document.getElementById("category-error-msg").hidden = false;
        }
        if(!costValid){
            document.getElementById("cost-error-msg").hidden = false;
        }
    }
}